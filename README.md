SAGE2
=======

VXLab fork of SAGE2 (https://bitbucket.org/sage2/sage2/).

Browser based implementation of SAGE. A cluster-based html viewer used for displaying elements across multiple browser windows.

##### Notice #####
SAGE and SAGE2 are trademarks of the University of Illinois Board of Trustees (SAGE™ and SAGE2™).